module gitlab.com/digitaldollar/bifrost/txscript

go 1.14

require (
	github.com/btcsuite/btcd v0.20.1-beta
	github.com/btcsuite/btclog v0.0.0-20170628155309-84c8d2346e9f
	github.com/btcsuite/btcutil v1.0.1
	github.com/davecgh/go-spew v1.1.1
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
)
